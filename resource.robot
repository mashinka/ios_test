*** Settings ***
Library              AppiumLibrary
Library              SeleniumLibrary
Library              XML
Library              Collections
Library              ListParser
Library              DateTime
Resource             resource.robot
Resource             controls.robot
Variables            ListParser.py
*** Variables ***
${REMOTE_URL}     http://0.0.0.0:4723/wd/hub
${platformName}  iOS
${platformVersion}  11.0
${xcodeSigningId}  iPhone Developer
${bundleId}    com.vispamedia.turbo.stage
${sendKeyStrategy}  setValue
${usePrebuiltWDA}  true
${wdaLocalPort}  8100
${xcodeOrgId}  VISPA MEDIA, OOO
${deviceName}  Iphone
${udid_iPhone7}  8bab36b32387df630d7a41d3b1309b5667539b12
${udid_iPhone7Plus}  87b25f63c04541ee074384880116eceac4f03d9f
${udid_iPhone6Plus}  870965dd92a1c41e7303005b5be83eb2979d095e

*** Keywords ***
Capture Page Screenshot
    AppiumLibrary.Capture Page Screenshot
get source
    AppiumLibrary.Get Source
log source
    AppiumLibrary.Log Source
get elements
    XML.Get Elements
looch open application
    open application    ${REMOTE_URL}   bundleId=${bundleId}  deviceName=Iphone  platformName=${platformName}   platformVersion=${platformVersion}  sendKeyStrategy=${sendKeyStrategy}   udid=${udid_iPhone7}  usePrebuiltWDA=true  wdaLocalPort=8100  xcodeOrgId=VISPA MEDIA   xcodeSigningId=iPhone Developer  newCommandTimeout=0   connectHardwareKeyboard=true     noReset=true    autoAcceptAlerts=true
    #run keyword and ignore error  click element at coordinates    375  31

click element
    [Arguments]       ${element}
    AppiumLibrary.Click Element     ${element}

input text
   [Arguments]    ${field}    ${text}
   AppiumLibrary.Input Text      ${field}    ${text}

close all
    close application
    Close All Browsers


*** Variables ***
${user_pass}   123456
${user}   t01


${MAIN_PAGE_URL}                                https://stage.web.vispamedia.com

*** Keywords ***
DisconectSocial
    [Arguments]                                 ${social_network}  ${my_auth_token}
    execute javascript                          fetch ('https://api.stage.vispamedia.com/social_login/${social_network}', {method: 'DELETE', credentials: 'include', headers: {cookie: '${my_auth_token}'}})

ConnectCosial
    [Arguments]                                  ${social_network}  ${my_auth_token}

DeleteAccount
    [Arguments]                                 ${user_name}
    SeleniumLibrary.Switch Browser               1
    Execute Async Javascript                    var callback = arguments[arguments.length - 1]; fetch ('https://api.stage.vispamedia.com/auth/delete', {headers: {'Accept': 'application/json','Content-Type': 'application/json'},method: 'POST', credentials: 'include', body: JSON.stringify({login: '${user_name}', password: '${user_pass}'})}).then(callback)
    Execute Javascript                          location.reload();
    SeleniumLibrary.Wait Until Page Contains Element             ${Login_Button}  timeout=10

ChannelFollow
    [Arguments]                                 ${target_id}  ${my_auth_token}
    execute javascript                          fetch ('https://api.stage.vispamedia.com/follow/channels/${target_id}', {method: 'POST', credentials: 'include', headers: {cookie: '${my_auth_token}'}})

Get Random Mail
    ${USER_RANDOM_MAIL} =  Get Current Date       result_format=%y%m%d%H%M%S@vispamedia.ru
    [Return]   ${USER_RANDOM_MAIL}

Get Random Name
    ${platform}=   Evaluate   sys.platform   sys
    ${USER_RANDOM_NAME} =  Get Current Date      result_format=${platform}_%y%m%d%H%M%S
    [Return]   ${USER_RANDOM_NAME}


WaitAndClickBrowser
    [Arguments]    ${element}
    SeleniumLibrary.Wait Until Page Contains Element  ${element}      timeout=10
    SeleniumLibrary.Click Element                     ${element}


WaitAndClick
    [Arguments]    ${element}
    AppiumLibrary.Wait Until Page Contains Element  ${element}      timeout=10
    AppiumLibrary.Click Element   ${element}


WaitForControlvisibleAndClick
    [Arguments]  ${btn_name}
    ${XML}=  AppiumLibrary.Log Source
    get element coordinate   ${XML}   ${btn_name}   y
    ${controll_coordinate}=  get element coordinate   ${XML}   ${btn_name}   y
    ${screen_size}=  screen size     ${XML}
    ${bottom}=    Evaluate     int(${screen_size}*0.8)
    ${top}=  Evaluate   int(${screen_size}*0.25)
            :FOR    ${CN_index}    IN RANGE  5
        \    ${XML}=  AppiumLibrary.Log Source
        \    ${visibility}=  run keyword and ignore error   get visibility   ${XML}  ${btn_name}
        \    log  ${visibility}
        \    Exit For Loop If                        ${visibility} == ('PASS', True)
        \    Run Keyword If                          ${visibility} == ('PASS', False)    swipe   0  ${bottom}  0  ${top}
     click element   ${btn_name}


Return element from massive
    [Arguments]  ${btn_name}   ${index}
    ${XML}=  AppiumLibrary.Log Source
    ${element}=  get element from massive   ${XML}  btn_carousel  ${index}
    [Return]   ${element}


Registration new user
    ${user_name}=  Get Random Name
    ${user_mail}=  Get Random Mail
    waitandclick   ${Tapbar_profile}
    waitandclick  id=btn_singup
    input text  id=text_signup_username        ${user_name}
    input text  id=text_signup_email           ${user_mail}
    input text  id=text_signup_password        ${user_pass}
    waitandclick  id=btn_signup_signup
    [Return]    ${user_name}



Login
    [Arguments]    ${user}
    waitandclick   ${Tapbar_profile}
    waitandclick   ${Log_In}
    input text     ${Log_in_username}         ${user}
    input text     ${Log_in_Password}         ${user_pass}
    click element  ${Log_in_login_btn}

LoginFB
    click element                       ${Tapbar_profile}
    click element                       id=btn_singin
    click element                       id=btn_login_social[fb]
    wait until page contains element    xpath=//XCUIElementTypeOther[@name="main"]/XCUIElementTypeTextField    10
    input text     xpath=//XCUIElementTypeOther[@name="main"]/XCUIElementTypeTextField              ${UserFB}
    input text     xpath=//XCUIElementTypeOther[@name="main"]/XCUIElementTypeSecureTextField        ${PasswordFB}
    click element  id=Log In
    run keyword and ignore error  wait until page contains element  xpath=//XCUIElementTypeButton[contains(@name,"Leo")]    10
    click element  xpath=//XCUIElementTypeButton[contains(@name,"Leo")]

ClearFB
    click element  ${Tapbar_profile}
    click element  id=row_account-settings



Log out
    waitandclick                                             ${Tapbar_profile}
    waitandclick                                             ${Profile_Settings}
    waitandclick                                             ${Profile_Settings_Logout}
    waitandclick                                             ${Profile_Settings_Logout_dialog_Logout}
    AppiumLibrary.Wait Until Page Does Not Contain Element   ${Profile_Settings_Logout_dialog_Logout}

GotoSearch
    waitandclick     ${Tapbar_search}


GotoAccountSettings
    waitandclick    ${Tapbar_profile}
    waitandclick    ${Profile_Account_Settings}


GotoCommunity
    waitandclick    ${Tapbar_profile}
    waitandclick    ${Profile_Community}

GotoSettings
    waitandclick    ${Tapbar_profile}
    waitandclick    ${Profile_Settings}

AccountSettingsSetAvatar
    GotoAccountSettings
    waitandclick  id=Change Picture
    waitandclick  id=From Camera…
    run keyword and ignore error  wait until page contains element  id=OK  3
    run keyword and ignore error  click element    id=OK
    waitandclick    id=PhotoCapture
    waitandclick    id=Use Photo
    waitandclick    id=Save
    wait until page contains element    id=row_cast


AccountSettingsAvatarRemove
    GotoAccountSettings
    waitandclick  id=Change Picture
    waitandclick  id=Remove Avatar
    waitandclick    id=Save
    wait until page does not contain element   id=Save


ClickOnSearchButton
    ${XML}=  AppiumLibrary.Log Source
    ${controll_x}=   get visible element param   ${XML}   Search   x
    ${controll_y}=   get visible element param     ${XML}   Search    y
    ${controll_width}=  get visible element param    ${XML}   Search  width
    ${controll_height}=  get visible element param    ${XML}   Search  height
    ${x}=  Evaluate     int(${controll_x}+${controll_width}*0.5)
    ${y}=  Evaluate     int(${controll_y}+${controll_height}*0.5)
    click element at coordinates  ${x}   ${y}
    #todo гриша добавить локатор на лоадер
    sleep  3



RegistrationBrowser
    ${email}=  Get Random Mail
    ${user_name}=  Get Random Name
    execute javascript                          fetch ('https://api.stage.vispamedia.com/auth/registration', {headers: {'Accept': 'application/vnd.api+json','Content-Type': 'application/vnd.api+json'},method: 'POST', credentials: 'include',body: JSON.stringify({data: {"type": "users", "attributes": {email: '${email}',username: '${user_name}', password: '${user_pass}'}}})}).then(data => data.json()).then((json) => {localStorage.token = json.meta.token; localStorage.id = json.data.id; location.reload(true)})
    :FOR    ${i}    IN RANGE  20
    \    ${auth_token}                          execute javascript  return localStorage.token
    \    Exit For Loop If                       '${auth_token}'!='None'
    \    Run Keyword If                         ${auth_token}==(None)  sleep  0.05
    ${id}                                       execute javascript  return localStorage.id
    ${user}                                     Create Dictionary    name={}  token={}  id={}
    Set To Dictionary                           ${user}  name  ${user_name}
    Set To Dictionary                           ${user}  token  ${auth_token}
    Set To Dictionary                           ${user}  id  ${id}
    log dictionary                              ${user}
    SeleniumLibrary.Wait Until Page Contains Element             css=[class*=e2e_usermenu]  timeout=10
    [Return]                                    ${user}


Browser Registration And Write Token
    SeleniumLibrary.Create Webdriver                             Firefox
    GoToStage
    ${userA}=   RegistrationBrowser
    write_to_file   ${UserA.token}
    [Return]  ${userA}



SelectFromSearcResult
    [Arguments]   ${anyElement}
    ${XML}=  AppiumLibrary.Log Source
    ${clickElement}=  random visible element from search  ${XML}   ${anyElement}
    log  ${clickElement}
    click element    id=${clickElement}


LogoutWeb
    execute javascript                          fetch ('https://api.stage.vispamedia.com/auth/logout', {headers: {'Accept': 'application/json','Content-Type': 'application/json'},method: 'POST', credentials: 'include'}).then(() => location.reload(true))
    wait until element is enabled               ${web_Login_Button}    timeout=10
    sleep                                       0.5

GoToStage
    ${platform}                                 Evaluate   sys.platform   sys
    run keyword if                              '${platform}' == 'win32'   Set Window Position   0    30
    run keyword if                              '${platform}' == 'darwin'  Set Window Position   2    2
    run keyword if                              '${platform}' == 'win32'   Set Window Size  1600  1000
    run keyword if                              '${platform}' == 'darwin'  maximize browser window
    Go To                                       ${MAIN_PAGE_URL}



GoToPage
    [Arguments]                                 ${page}
    go to                                       ${main_page_url}/${page}
    Wait Until Keyword Succeeds               15 sec   3 sec      location should contain             ${page}
    SeleniumLibrary.Wait Until Element Is Visible               css=.e2e_icon_logo
    sleep    1



GotoUserChannel
   waitandclick                       ${tapbar_profile}
   waitandclick                       ${profile_userchannel_btn}

ChatMenuSelectUserlist
   waitandclick                       ${channel_menu_btn}
   waitandclick                       ${Channel_Userlist_btn}

RegistrationSocial_Web
    [Arguments]                                 ${social_network}  ${user_mail}    ${user_name}
    WaitAndClick                                ${User_Menu}
    WaitAndClick                                ${SignInPopup_SignIn_To_SignUp}
    WaitAndClick                                css=[class*=e2e_${social_network}button]
    SocialWindow                                ${social_network}
    SeleniumLibrary.wait until element is visible  css=[class*=AuthSocialPopup__form]  timeout=10
    WaitAndClick                                css=[class*=AuthSocialPopup__tabsWrapper] > [class*=AuthSocialPopup__tab]:nth-child(1)
    Input Text                                  css=[class*=AuthSocialPopup__form] [class*=CommonInput__input]:nth-of-type(1)   ${user_mail}
    Input Text                                  css=[class*=CommonInput__field]:nth-child(2) > input:nth-child(1)   ${user_name}
    WaitAndClickBrowser                         css=[class*=AuthSocialPopup__submitButton]
    SeleniumLibrary.Wait Until Page Contains Element            css=.e2e_avatar
    ${id}                                       execute javascript  return localStorage.id
    ${user}                                     Create Dictionary    name={}  token={}  id={}
    Set To Dictionary                           ${user}  name  ${user_name}
    Set To Dictionary                           ${user}  token  ${auth_token}
    Set To Dictionary                           ${user}  id  ${id}
    log dictionary                              ${user}
    SeleniumLibrary.Wait Until Page Contains Element       css=[class*=e2e_usermenu]  timeout=10
    [Return]                                    ${user}


