*** Settings ***
#Library              AppiumLibrary
Library              XML
Resource             resource.robot
Resource             controls.robot
#Test Setup           open application looch
Library              Collections
Library              ListParser
Library              DateTime
Library              SeleniumLibrary
#Test Setup          Create Webdriver      Chrome   #RegistrationBrowser and LoginApp
Test Teardown        Close Browsers Delete Account
#Test Teardown       close all
*** Variables ***


*** Keywords ***
Facebook Connect
    click element                              ${Profile_Account_Settings_FB_btn}
    AppiumLibrary.Wait Until Page Contains Element           ${facebook_mail_field}  timeout=15
    input text                                 ${facebook_mail_field}      ${facebook_mail}
    input text                                 ${facebook_password_field}  ${facebook_password}
    click element                              ${facebook_login_btn}
    run keyword and ignore error               waitandclick                            ${Facebook_continue_as}




Facebook Disсonnect
    AppiumLibrary.click element                               ${facebook_connected_btn}
    waitandclick                                              ${facebook_unlink}
    AppiumLibrary.wait until page does not contain element    ${facebook_connected_btn}
    waitandclick                                              ${Profile_Account_Settings_Save}




RegistrationBrowser and LoginApp
    ${userA}=   RegistrationBrowser
    looch open application
    Login     ${UserA}
    [Return]  ${userA}

Open Application And Login And FBConnect
    [Arguments]   ${userName}
    looch open application
    Login     ${userName}
    GotoAccountSettings
    Facebook Connect


WriteInChatAndSend
    [Arguments]                                 ${textInput}
    #SeleniumLibrary.Wait Until Page Contains Element            ${Chat_Menu}   timeout=30
    #${selector_name}                            Fetch From Right  ${Chat_Input}  css=
    #Execute JavaScript                          document.querySelector(`${selector_name}`).scrollIntoView(true);
    input text                                  ${Chat_Input}   ${textInput}
    WaitAndClick                                ${Chat_Submit_Button}


Close Browsers Delete Account
    Log out
    ${list}=  read_from_file
    ${Length}=  Get Length  ${list}

#    ${index}=  Evaluate   int(0)
    :FOR    ${i}    IN RANGE  ${Length}
    \    ${userDel}=  Get From List 	${list}    ${i}
    \    Run Keyword     DeleteAccount   ${userDel}

    delete file
    SeleniumLibrary.Close All Browsers



*** Test Cases ***
#todo С1313	Аккаунт: Настройки: Facеbook - подключение
Test-1313-AccountSettingsFBConnect
    [Tags]                                      case_id_1313
    ${userA}=  Browser Registration And Write Token
    Open Application And Login And FBConnect                ${UserA.name}
    AppiumLibrary.Page Should Contain Element               ${facebook_connected_btn}
    click element                                           ${profile_account_settings_save}


#todo С1315	Аккаунт: Настройки: Facebook - подключение уже подключенного к другому аккаунту
Test-1315-AccountSettingsFBConnect
    [Tags]                                      case_id_1315
    ${userA}=  Browser Registration And Write Token
    Open Application And Login And FBConnect                ${UserA.name}
    waitandclick  ${Profile_Account_Settings_Save}
    log out

    ${userB}=  Browser Registration And Write Token
    Open Application And Login And FBConnect                 ${userB.name}
    AppiumLibrary.Wait Until Page Contains Element           ${Facebook_Error}  timeout=10
    AppiumLibrary.Click Element                              ${Facebook_OK}
    waitandclick  ${Profile_Account_Settings_Save}


Test-1314-AccountSettingsFBConnect
    [Tags]                                                  case_id_1314   fff
    ${userA}=  Browser Registration And Write Token
    Open Application And Login And FBConnect                ${UserA.name}
    waitandclick                                            ${Profile_Account_Settings_Save}
    GotoAccountSettings
    Facebook Disсonnect


















