*** Settings ***
Library              AppiumLibrary
Library              XML
Library              Collections
Library              ListParser
Resource             resource.robot
Resource             controls.robot
Test Setup           looch open application
Test Teardown        close application
Variables            ListParser.py
*** Variables ***

*** Keywords ***
ClearSearch
    ${isClearBunnton}=  RUN KEYWORD AND IGNORE ERROR   page should contain element  ${Search_clear_resent}
    Run Keyword If  ${isClearBunnton} == ('PASS', True)     click element    ${Search_clear_resent}

*** Test Cases ***


Test-1177-SearchResult
   [Tags]        case_id_1177
   GotoSearch
   input text       ${search_line}   dota
   ClickOnSearchButton
   page should contain element   ${Search}


Test-1178-SearchGamesTapGame
   [Tags]        case_id_1178
   GotoSearch
   input text       ${search_line}   dota
   ClickOnSearchButton
   SelectFromSearcResult   ${Search_anyGame}
   page should contain element  id=Live


Test-1179-SearchGamesSeeAll
   [Tags]        case_id_1179
   GotoSearch
   input text       ${search_line}   dot
   #ClickOnSearchButton
   Hide Keyboard    key_name=Search
   waitandclick      ${search_games_seeall}
   page should contain element     ${Search_back}


#todo С1493	Поиск: Games: See All: поиск по играм   ???
Test-1493-SearchGamesResult
   [Tags]        case_id_1493
   GotoSearch
   input text       ${search_line}   dota
   ClickOnSearchButton
   page should contain element     ${search_games_seeall}


Test-1180-SearchChannelsUserClick
   [Tags]        case_id_1180   jjj
   GotoSearch
   input text       ${search_line}   dota
   ClickOnSearchButton
   sleep  5
   SelectFromSearcResult   ${Search_anyChannel}
   page should contain element     ${Channel_login_to_chat}

Test-1181-SearchChannelsSeeAll
   [Tags]        case_id_1180   jjj
   GotoSearch
   input text       ${search_line}   dota
   ClickOnSearchButton
   sleep  5
   SelectFromSearcResult    ${search_anychannel}
   page should contain element      ${Channel_login_to_chat}
#todo С1494	Поиск: Channels: See All: поиск по каналам   ???

#todo С1506	Поиск: Channels: Подписаться из окна поиска (login/logout) Гриша


Test-1795-SearchLiveTap
   [Tags]        case_id_1795    jjj
   GotoSearch
   input text       ${search_line}   first
   ClickOnSearchButton
   sleep  5

   SelectFromSearcResult    ${Search_anyLive}
   page should contain element     ${Channel_login_to_chat}


Test-1796-SearchLiveSeeAll
   [Tags]        case_id_1796    ggg
   GotoSearch
   input text       ${search_line}   first
   ClickOnSearchButton
   sleep  5
   WaitForControlvisibleAndClick    ${Search_streams_seeall}
   page should contain element     ${search_back}

#todo С1797	Поиск: Streams: See All: поиск по стримам    ???
Test-1182-SearchVoSelect
   [Tags]        case_id_1182   jjj
   GotoSearch
   input text       ${search_line}   first
   ClickOnSearchButton
   sleep  5
   SelectFromSearcResult     ${Search_anyVideo}
   page should contain element     ${Channel_login_to_chat}



Test-1183-SearchVoDSeeAll
   [Tags]        case_id_1183
   GotoSearch
   input text       ${search_line}   first
   ClickOnSearchButton
   sleep  5
   WaitForControlvisibleAndClick     ${Search_videos_seeall}
   page should contain element     ${Search_back}




#todo С1495	Поиск: VoD: See All: поиск по водам   ???

Test-1496-SearchRecentlyRequest
   [Tags]        case_id_1496
   GotoSearch

   ClearSearch
   input text                       ${search_line}   dota
   ClickOnSearchButton
   click element                    ${Search_clear}
   page should contain element      ${Search_recent_dota}


Test-1497-SearchRecentlyRequestClear
   [Tags]        case_id_1497
   GotoSearch
   ClearSearch
   input text                       ${search_line}   dota
   ClickOnSearchButton
   ClearSearch
   page should not contain element      ${Search_recent_dota}









