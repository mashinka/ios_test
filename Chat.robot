*** Settings ***
Library              AppiumLibrary
Library              XML
Library              Collections
Library              ListParser
Library              SeleniumLibrary
Resource             resource.robot
Resource             controls.robot
Test Setup           looch open application
Test Teardown         close all
Variables            ListParser.py


*** Keywords ***
Sent text into chat
    [Arguments]       ${text}
    AppiumLibrary.wait until page contains element   ${Channel_SaySomething_Field}
    input text                                       ${Channel_SaySomething_Field}               ${text}
    WaitAndClick                                     ${Channel_Send_btn}
    click element                                    ${Channel_Send_btn}

Sent smile into chat
    [Arguments]       ${text}
    wait until page contains element   ${Channel_SaySomething_Field}
    input text                    ${Channel_SaySomething_Field}               ${text}
    waitandclick                  ${Channel_Send_btn}


Browser Registration GoTo Page
    [Arguments]           ${UserChannel}
    Browser Registration
    GoToPage                   {UserChannel}


Browser Registration
    Create webdriver           Chrome
    GoToStage
    ${username}=               RegistrationBrowser
        [Return]                   ${username}


Looch Open And Login
    looch open application
    #Login   t01


page should contain element
    [Arguments]     ${element}
    AppiumLibrary.Page Should Contain Element   ${element}

WriteInChatAndSend
    [Arguments]                                 ${textInput}
    SeleniumLibrary.Wait Until Page Contains Element            ${web_Channel_Chat_Menu}     timeout=30
    #${selector_name}                            Fetch From Right  ${web_Channel_Chat_Input}  css=
    #Execute JavaScript                          document.querySelector(`${selector_name}`).scrollIntoView(true);
    SeleniumLibrary.Input Text                  ${web_Channel_Chat_Input}   ${textInput}
    WaitAndClickBrowser                                ${web_Channel_Chat_Submit_Button}



*** Test Cases ***
#todo С1215	Канал стримера: чат (login/logout)
Test-1215-ChatLogin
   [Tags]        case_id_1215
   Registration new user
   waitandclick    ${Tapbar_profile}
   waitandclick    ${Profile_Account_Settings}
   waitandclick    ${profile_userchannel_btn}
   page should contain element   ${Search}

#todo С1264	Мой канал: Chat: иконка владельца возле своих сообщений
Test-1264-ChatMessageicon
   [Tags]                             case_id_1264
   Login                              ${user}
   waitandclick                       ${profile_userchannel_btn}
   Sent text into chat
   Sent text into chat
   Sent text into chat
   #todo добавить иконку для сообщения




#todo С1265	Мой канал: Chat: выдача администраторских прав
Test-1265-ChatMessageicon
   [Tags]                             case_id_1265    hhh
   ${username}=                       Browser Registration
   GoToPage                           t01
   WriteInChatAndSend                 test
   looch open application
   waitandclick                       ${tapbar_profile}
   waitandclick                       ${profile_userchannel_btn}
   waitandclick                       ${channel_menu_btn}
   waitandclick                       ${Channel_Userlist_btn}
   waitandclick                       ${username}
   waitandclick                       ${Channel_UserMenu_Admin_btn}
   page should contain element        ${Channel_UserMeny_Admin_icon}
   sleep    10







#todo С1266	Мой канал: Chat: лишение администраторских прав
Test-1266-ChatMessageicon
   [Tags]                             case_id_1265    hhh
   ${username}=                       Browser Registration
   GoToPage                           t01
   WriteInChatAndSend                 test
   looch open application
   waitandclick                       ${tapbar_profile}
   waitandclick                       ${profile_userchannel_btn}
   waitandclick                       ${channel_menu_btn}
   waitandclick                       ${Channel_Userlist_btn}
   waitandclick                       ${username}
   waitandclick                       ${Channel_UserMenu_Admin_btn}
   page should contain element        ${Channel_UserMeny_Admin_icon}
   waitandclick                       ${username}
   waitandclick                       ${Channel_UserMenu_Admin_btn}
   page should not contain element    ${Channel_UserMeny_Admin_icon}

#todo С1268	Мой канал: Chat: /ban username - забаненый пользователь не может писать
Test-1268-ChatBanPassiveCantWrite
   [Tags]                             case_id_1265    hhh
   ${username1}=                       Browser Registration
   ${username2}=                       Browser Registration
   GoToPage                           ${username1}
   #todo бан на вебе
   looch open application
   login                              ${username1}
   #todo go to ${username2}
   #todo cant write

#todo С1269	Мой канал: Chat: /mute username <seconds>- сообщение на стороне актива
Test-1269-ChatBanPassiveCantWrite
   [Tags]                             case_id_1265    hhh
   ${username}=                       Browser Registration
   GoToPage                           t01
   looch open application
   waitandclick                       ${tapbar_profile}
   waitandclick                       ${profile_userchannel_btn}
   waitandclick                       ${channel_menu_btn}
   waitandclick                       ${Channel_Userlist_btn}
   waitandclick                       ${username}
   waitandclick                       ${Channel_UserMenu_ban_btn}
   #todo close user menu
   #todo check msg

#todo С1219	Канал стримера: чат: отправка сообщения
Test-1266-ChatMessageicon
   [Tags]                             case_id_1265    lll
   ${text}=   get random name
   waitandclick                       ${tapbar_profile}
   waitandclick                       ${profile_userchannel_btn}
   sent text into chat                ${text}
   hide keyboard
   ${value}=  AppiumLibrary.Get Element Attribute     ${Channel_Chat_msg_cell}  value
   should be equal    ${value}  ${text}

#todo С1220	Канал стримера: чат: отправка смайлов
#todo С1518	Канал стримера: чат: иконки возле сообщений


#todo С1221	Канал стримера: чат: User list - переход
Test-1269-ChatBanPassiveCantWrite
   [Tags]                             case_id_1265    hhh
   looch open application
   waitandclick                       ${tapbar_profile}
   waitandclick                       ${profile_userchannel_btn}
   waitandclick                       ${channel_menu_btn}
   waitandclick                       ${Channel_Userlist_btn}
   #todo check is  user menu open


#todo С1222	Канал стримера: чат: User list - поиск пользователя
Test-1222-ChatUserMenuSearch
   [Tags]                             case_id_1269    hhh
   ${username}=                       Browser Registration
   GoToPage                           t01
   looch open application
   GotoUserChannel
   ChatMenuSelectUserlist
   #todo search user
   #todo check msg

#todo С1223	Канал стримера: чат: User list - закрыть
Test-1223-ChatUserMenuListClose
   [Tags]                             case_id_1269    hhh
   ${username}=                       Browser Registration
   GoToPage                           t01
   looch open application
   GotoUserChannel
   ChatMenuSelectUserlist
   #todo user list close
#todo С1226	Канал стримера: чат: Settings - переход
Test-1226-ChatUserMenuListClose
   [Tags]                             case_id_1269    hhh
   ${username}=                       Browser Registration
   GoToPage                           t01
   looch open application
   GotoUserChannel
   #todo Settigs open
   #todo Settings close



#todo С1227	Канал стримера: чат: Settings - закрыть
Test-1227-ChatUserMenuSettingsClose
   [Tags]                             case_id_1269    hhh
   ${username}=                       Browser Registration
   GoToPage                           t01
   looch open application
   GotoUserChannel
   #todo Settigs open
   #todo Settings close


#todo С1228	Канал стримера: чат: Settings - размер шрифта
Test-1228-ChatUserMenuSettingsFontSize
   [Tags]                             case_id_1269    hhh
   ${username}=                       Browser Registration
   GoToPage                           t01
   looch open application
   GotoUserChannel
   #todo Settigs open
   #todo Set font size


#todo С1229	Канал стримера: чат: Settings - timestamps
Test-1229-ChatUserMenuSettings
   [Tags]                             case_id_1269    hhh
   ${username}=                       Browser Registration
   GoToPage                           t01
   looch open application
   GotoUserChannel
   #todo Settigs open
   #todo Set timestamp
