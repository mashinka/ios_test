*** Settings ***
Library              AppiumLibrary
Library              XML
Library              Collections
Library              ListParser
Resource             resource.robot
Resource             controls.robot
Test Setup           open application looch
Test Teardown        close application
Variables            ListParser.py
*** Variables ***
${REMOTE_URL}     http://0.0.0.0:4723/wd/hub
${platformName}  iOS
${platformVersion}  11.0
${xcodeSigningId}  iPhone Developer
${bundleId}  tv.stage
${sendKeyStrategy}  setValue
${usePrebuiltWDA}  true
${wdaLocalPort}  8100
${xcodeOrgId}  VISPA MEDIA, OOO
${deviceName}  Iphone
${udid_iPhone7}  8bab36b32387df630d7a41d3b1309b5667539b12
${udid_iPhone7Plus}  87b25f63c04541ee074384880116eceac4f03d9f
${udid_iPhone6Plus}  870965dd92a1c41e7303005b5be83eb2979d095e

*** Keywords ***
Capture Page Screenshot
    AppiumLibrary.Capture Page Screenshot
get source
    AppiumLibrary.Get Source
log source
    AppiumLibrary.Log Source
get elements
    XML.Get Elements
open application looch
    open application   ${REMOTE_URL}   bundleId=${bundleId}  deviceName=Iphone  platformName=${platformName}   platformVersion=${platformVersion}  sendKeyStrategy=${sendKeyStrategy}   udid=${udid_iPhone6Plus}  usePrebuiltWDA=true  wdaLocalPort=8100  xcodeOrgId=VISPA MEDIA   xcodeSigningId=iPhone Developer  newCommandTimeout=0   connectHardwareKeyboard=true   fullReset=false   noReset=false    autoAcceptAlerts=true
    run keyword and ignore error  click element at coordinates    375  31


*** Test Cases ***
Test-1169-CarouselSwipe
    [Tags]                                      case_id_1169
    open application looch
    ${start_first_element}=   Return element from massive   btn_carousel  0
    ${start_second_element}=  Return element from massive   btn_carousel  2
    scroll    id=${start_first_element}   id=${start_second_element}
    ${end_first_element}=     Return element from massive   btn_carousel  0
    should not be equal    ${start_first_element}   ${end_first_element}



Test-1170-CarouselSelectUser
    [Tags]                                      case_id_1170
    open application looch
    ${carousel_element}=   Return element from massive   btn_carousel  0
    click element    id=${carousel_element}
    #todo id для плашки с follow/unfollow id проставить


Test-1171-BestGamesSelectGame
    [Tags]                                      case_id_1171
    open application looch
    ${XML}=  AppiumLibrary.Log Source
    ${random_visible_bestgame}=  random visible element  ${XML}  btn_bestgames
    click element   id=${random_visible_bestgame}
    page should contain element  id=Live


Test-1491-BestGamesSeeAll
    [Tags]                                      case_id_1491
    open application looch
    click element  id=btn_bestgames_seeall
    page should contain element   name=Games


Test-1173-TopChannelsSelect
    [Tags]                                      case_id_1173
    open application looch
    ${XML}=  AppiumLibrary.Log Source
    ${random_visible_bestgame}=  random invisible element  ${XML}  btn_topchannels
    WaitForControlvisibleAndClick   ${random_visible_bestgame}
    #todo id для плашки с follow/unfollow id проставить


Test-1174-TopChannelsSeeAll
    [Tags]                                      case_id_1174
    open application looch
    ${XML}=  AppiumLibrary.Log Source
    WaitForControlvisibleAndClick   btn_topchannels_seeall
    page should contain element   name=Channels


Test-1175-NewChannelsSelect
    [Tags]                                      case_id_1175
    open application looch
    sleep  1
    ${XML}=  AppiumLibrary.Log Source
    ${random_visible_bestgame}=  random invisible element  ${XML}   btn_newchannels
    WaitForControlvisibleAndClick   ${random_visible_bestgame}
    #todo id для плашки с follow/unfollow id проставить


Test-1176-NewChannelsSeeAll
    [Tags]                                      case_id_1176
    open application looch
    ${XML}=  AppiumLibrary.Log Source
    WaitForControlvisibleAndClick   btn_newchannels_seeall
    page should contain element   name=Channels

Test-1184-GotoChannels
    [Tags]                                      case_id_1184
    open application looch
    click element   id=tаbbar[channels]
    page should contain element   name=Channels


Test-1350-GotoChannelsNew
    [Tags]                                      case_id_1350
    open application looch
    click element   id=tаbbar[channels]
    click element    id=New
    ${value}=  AppiumLibrary.Get Element Attribute     id=New  value
    should be equal    ${value}  1


Test-1775-GotoChannelsUserIsNotLogin
    [Tags]                                      case_id_1775
    open application looch
    click element   id=tаbbar[channels]
    click element    id=Following
    ${value}=  AppiumLibrary.Get Element Attribute     id=Following  value
    page should contain element  id=Log In
    #todo id=Log In

#todo С1776	Channels: Подписки: юзер не залогинен - логин

#todo С1777	Channels: Подписки: у пользователя нет подписок

#todo С1778	Channels: Подписки: у пользователя есть подписки
#todo С1782	Channels: Подписки: у пользователя есть подписки: тап на онлайнстрим
#todo С1783	Channels: Подписки: у пользователя есть подписки: тап на любого из списка



Test-1187-GotoGames
    [Tags]                                      case_id_1187
    open application looch
    click element   id=tаbbar[games]
    page should contain element   name=Games

#todo С1729	Games: Выбор игры

#todo С1188	Games: Live: Выбор стрима
#todo С1189	Games: Video: Выбор вода


#todo С1201	Канал стримера: Стрим: Pause
Test-1201-StreamPause
    [Tags]                                      case_id_1201
    open application looch
    ${carousel_element}=   Return element from massive   btn_carousel  0
    click element    id=${carousel_element}


#todo С1501	Канал стримера: информация о канале
#todo С1502	Канал стримера: информация о канале: инфопанели
#todo С1503	Канал стримера: информация о канале: анонсы: выбор анонса


#todo С1233	Канал стримера: videos: выбор видео
#todo С1232	Канал стримера: videos: See All
#todo С1505	Канал стримера: videos: See All: выбор видео

#todo С1507	Мой канал: информация о канале
#todo С1508	Мой канал: информация о канале: инфопанели
#todo С1509	Мой канал: информация о канале: анонсы: выбор анонса

#todo С1510	Мой канал: информация о канале: анонсы: See All
#todo С1519	Мой канал: информация о канале: анонсы: See All: Выбор анонса
#todo С1511	Мой канал: videos: выбор видео
#todo С1512	Мой канал: videos: See All
#todo С1513	Мой канал: videos: See All: выбор видео


#todo С1166	Аккаунт: Логин существующим пользователем
Test-1245-Login
    [Tags]                                      case_id_1245
    open application looch
    Login   ${user}
    page should contain element   id=${user}

#todo С1520	Аккаунт: Логин: Восстановление пароля
Test-1245-Login
    [Tags]                                      case_id_1245
    open application looch
    Login   ${user}
    page should contain element   id=${user}
#todo С1516	Аккаунт: Логин: Terms and Conditions и Privacy Policy (ру/ен)


Test-1245-Registration
    [Tags]                                      case_id_1245
    open application looch
    ${user_name}=  Registration new user
    page should contain element   id=${user_name}




#todo С1517	Аккаунт: Регистрация: Terms and Conditions и Privacy Policy (ру/ен)
#todo С1246	Аккаунт: Регистрация нового пользователя через VK
#todo С1514	Аккаунт: Регистрация нового пользователя через уже подключенный VK
#todo С1247	Аккаунт: Регистрация нового пользователя через FB
Test-1247-RegistrationFB
    [Tags]                                      case_id_1247
    open application looch
    LoginFB
    page should contain element   id=${user_name}
    #TOdo ye;ty ntcnjdsq gjkmpj
#todo С1515	Аккаунт: Регистрация нового пользователя через уже подключенный FB




