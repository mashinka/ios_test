*** Settings ***
Library              AppiumLibrary
Library              XML
Library              Collections
Library              ListParser
Library              SeleniumLibrary
Resource             resource.robot
Resource             controls.robot
Test Setup           looch open application
Test Teardown         close all
Variables            ListParser.py


*** Keywords ***



Browser Registration GoTo Page
    [Arguments]           ${UserChannel}
    Browser Registration
    GoToPage                   {UserChannel}


Browser Registration
    Create webdriver           Chrome
    GoToStage
    ${username}=               RegistrationBrowser
        [Return]                   ${username}



page should contain element
    [Arguments]     ${element}
    AppiumLibrary.Page Should Contain Element   ${element}

WriteInChatAndSend
    [Arguments]                                 ${textInput}
    SeleniumLibrary.Wait Until Page Contains Element            ${web_Channel_Chat_Menu}     timeout=30
    #${selector_name}                            Fetch From Right  ${web_Channel_Chat_Input}  css=
    #Execute JavaScript                          document.querySelector(`${selector_name}`).scrollIntoView(true);
    SeleniumLibrary.Input Text                  ${web_Channel_Chat_Input}   ${textInput}
    WaitAndClickBrowser                                ${web_Channel_Chat_Submit_Button}



*** Test Cases ***
Test-1184-GotoChannels
    [Tags]                                      case_id_1184
    click element   id=tаbbar[channels]
    page should contain element   name=Channels
#TODO 1186

Test-1774-GotoChannelsFollowing
    [Tags]                                      case_id_1774
    click element   id=tаbbar[channels]
    click element    id=Following
    ${value}=  AppiumLibrary.Get Element Attribute     id=Following  value
    should be equal    ${value}  1



Test-1780-GotoChannelsTop
    [Tags]                                      case_id_1780
    click element   id=tаbbar[channels]
    click element    id=Top
    ${value}=  AppiumLibrary.Get Element Attribute     id=Top  value
    should be equal    ${value}  1

