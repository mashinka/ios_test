*** Settings ***
Library              AppiumLibrary
Library              XML
Library              Collections
Library              ListParser
Resource             resource.robot
Resource             controls.robot
Test Setup           looch open application
Test Teardown        close application
Suite Setup          open application and register new user
Suite Teardown       log out looch
Variables            ListParser.py
*** Variables ***

*** Keywords ***
log out looch
   looch open application
   Log out

open application and register new user
    looch open application
    registration new user
*** Test Cases ***
Test-1306-Avatar
    [Tags]                                      case_id_1306
    AccountSettingsSetAvatar


Test-1307-Avatarchange
    [Tags]                                      case_id_1307
    AccountSettingsSetAvatar
    AccountSettingsSetAvatar

Test-1308-Avatar
    [Tags]                                      case_id_1308
    AccountSettingsSetAvatar
    AccountSettingsAvatarRemove

#todo С1309	Аккаунт: Настройки: Смена имени пользователя
Test-1309-AccountSettingsChangeName
    [Tags]                                      case_id_1308
    GotoAccountSettings
    input text                                 ${Profile_Account_Settings_Username}   1
    Waitandclick                               ${Profile_Account_Settings_Save}
#todo С1310	Аккаунт: Настройки: Подтверждение mail



#todo С1311	Аккаунт: Настройки: Cмена mail
#todo С1312	Аккаунт: Настройки: Сброс пароля.


Test-1168-ProfileFeedbackSent
    [Tags]                                    case_id_1168
    waitandclick                               ${Tapbar_profile}
    waitandclick                              ${Profile_Feedback}
    waitandclick                              ${feedback_subject_select}
    waitandclick                              ${feedback_feedback}
    input text                                ${Feedback_Text_Field}   Some_text
    waitandclick                              ${Feedback_Submit}
    wait until page contains element          ${Feedback_Confirmation}


Test-1200-ProfileSettingsChangeLanguage
    [Tags]                                      case_id_1200
    waitandclick                              ${Tapbar_profile}
    waitandclick                              ${Profile_Settings}
    waitandclick                              ${Profile_Settings_Language_English}
    waitandclick                              ${Profile_Settings_Language_Russian}
    wait until page does not contain element  ${Profile_Settings_Language_English}
    waitandclick                              ${Profile_Settings_Language_Russian}
    waitandclick                              ${Profile_Settings_Language_English}
    wait until page does not contain element  ${Profile_Settings_Language_Russian}


Test-1190-ProfileSettingsChangeLanguage
    [Tags]                                      case_id_1190   ggg
    waitandclick                              ${Tapbar_profile}
    waitandclick                              ${Profile_Settings}
    waitandclick                              ${Settings_About_Looch_btn}
    wait until page contains element          ${settings_about_looch_page}


Test-1192-SettingsPrivacyPolicy
    [Tags]                                      case_id_1192   ggg
    waitandclick                              ${Tapbar_profile}
    waitandclick                              ${Profile_Settings}
    waitandclick                              ${Settings_PP_btn}
    wait until page contains element          ${settings_about_looch_pp_page}

Test-1191-SettingsToS
    [Tags]                                      case_id_1191   ggg
    waitandclick                              ${Tapbar_profile}
    waitandclick                              ${Profile_Settings}
    waitandclick                              ${Settings_ToS_btn}
    wait until page contains element          ${Settings_About_Looch_ToS_page}

Test-1197-GotoChannelsFollowing
    [Tags]                                      case_id_1197
    Log out
    Login                 ${user}


