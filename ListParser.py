from lxml import etree
from random import random
from os import  remove


def get_elements(text, name, visible="true"):
    tree = etree.fromstring(text.encode('utf8'))
    xml_result = tree.xpath(r".//*[contains(@name,'" + name + "[')][contains(@visible, '" + visible + "')]")
    return xml_result


def random_visible_element(text, btn_name, visible="true"):
    xml_result = get_elements(text, btn_name, visible)
    string_result = [el.get('name') for el in xml_result]
    return random.choice(string_result)


def random_visible_element_from_search(text, btn_name):
    return random_visible_element(text, btn_name + "[")


def random_invisible_element(text, btn_name):
    return random_visible_element(text, btn_name, visible="false")


def get_invisible_element_coordinate(text, btn_name, x_or_y):
    xml_result = get_elements(text, btn_name)
    string_result = [el.get(x_or_y) for el in xml_result]
    return string_result[0]


def get_visible_element_param(text, btn_name, x_or_y):
    xml_result = get_elements(text, btn_name)
    string_result = [el.get(x_or_y) for el in xml_result]
    return string_result[0]


def screen_size(text):
    
    tree = etree.fromstring(text.encode('utf8'))
    xml_result = tree.xpath(r".//*[contains(@type,'XCUIElementTypeWindow')]")
    string_result = [el.get('height') for el in xml_result]
    return string_result[0]


def get_visibility(text, btn_name):
    btn_name = btn_name.split('id=')[1]
    tree = etree.fromstring(text.encode('utf8'))
    xml_result = tree.xpath(r".//*[contains(@name,'" + btn_name + "')]")
    string_result = [el.get('visible') for el in xml_result]
    if string_result[0] == 'true':
        return True
    else:
        return False


def get_element_coordinate(text, btn_name, x_or_y):
    btn_name = btn_name.split('id=')[1]
    tree = etree.fromstring(text.encode('utf8'))
    xml_result = tree.xpath(r".//*[contains(@name,'" + btn_name + "')]")
    string_result = [el.get(x_or_y) for el in xml_result]
    return string_result[0]


def get_element_from_massive(text, btn_name, index):
    index = int(index)
    tree = etree.fromstring(text.encode('utf8'))
    xml_result = tree.xpath(r".//*[contains(@name,'" + btn_name + "')]")
    string_result = [el.get('name') for el in xml_result]
    return string_result[index]


def write_to_file(userName):
    with open("foo.txt", "a+") as fo:
        fo.write(userName + '\n')


def read_from_file():
    with open("foo.txt", "r+") as fo:
        user_list = [line.strip() for line in fo]
        return user_list


def delete_file():
    remove("foo.txt")

