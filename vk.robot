*** Settings ***
Library              AppiumLibrary
Library              XML
Resource             resource.robot
Resource             controls.robot
Library              Collections
Library              ListParser
Library              DateTime
Library              SeleniumLibrary
Test Teardown        Close Browsers Delete Account
#Test Teardown       close all
*** Variables ***


*** Keywords ***
VK Sign In
    AppiumLibrary.Wait Until Page Contains Element  ${VK_mail_field}  timeout=15
    input text                                      ${VK_mail_field}      ${userVK}
    input text                                      ${VK_password_field}  ${passwordVK}
    click element                                    ${VK_login_btn}
    AppiumLibrary.Wait Until Page Does Not Contain Element                 ${VK_login_btn}

VK Connect
    click element                                   ${Profile_Account_Settings_VK_btn}
    VK Sign In


Capture Page Screenshot
    AppiumLibrary.Capture Page Screenshot



VK Disсonnect
    WaitAndClick                                              ${VK_connected_btn}
    waitandclick                                              ${VK_unlink}
    AppiumLibrary.wait until page does not contain element    ${VK_connected_btn}
    waitandclick                                              ${Profile_Account_Settings_Save}


Open Application And Login And VKConnect
    [Arguments]   ${userName}
    looch open application
    Login     ${userName}
    GotoAccountSettings
    VK Connect

RegistrationBrowser and LoginApp
    ${userA}=   RegistrationBrowser
    looch open application
    Login     ${UserA}
    [Return]  ${userA}


Close Browsers Delete Account
    Log out
    ${list}=  read_from_file
    ${Length}=  Get Length  ${list}

#    ${index}=  Evaluate   int(0)
    :FOR    ${i}    IN RANGE  ${Length}
    \    ${userDel}=  Get From List 	${list}    ${i}
    \    Run Keyword     DeleteAccount   ${userDel}

    delete file
    SeleniumLibrary.Close All Browsers




*** Test Cases ***
Test-1356-AccountSettingVKConnect
    [Tags]                                      case_id_1356
    ${userA}=  Browser Registration And Write Token
    Open Application And Login And VKConnect  ${UserA.name}
    waitandclick                              ${Profile_Account_Settings_Save}

Test-1357-AccountSettingsVKConnect
    [Tags]                                      case_id_1357
    ${userA}=  Browser Registration And Write Token
    Open Application And Login And VKConnect  ${UserA.name}
    waitandclick  ${Profile_Account_Settings_Save}
    log out

    ${userB}=  Browser Registration And Write Token
    Open Application And Login And VKConnect  ${UserB.name}
    AppiumLibrary.Wait Until Page Contains Element           ${VK_Error}
    AppiumLibrary.Click Element                              ${VK_OK}
    waitandclick  ${Profile_Account_Settings_Save}


Test-1358-AccountSettingsVKConnect
    [Tags]                                         case_id_1358   fff
    ${userA}=  Browser Registration And Write Token
    Open Application And Login And VKConnect  ${UserA.name}
    waitandclick                                   ${Profile_Account_Settings_Save}
    GotoAccountSettings
    VK Disсonnect



##todo	C1246	Аккаунт: Регистрация нового пользователя через VK
Test-1246-Sing_Up_VK_new
    [Tags]                                         case_id_1246   fff
    looch open application
    WaitAndClick   ${Tapbar_profile}
    WaitAndClick   ${Sing_Up}
    WaitAndClick   ${Sing_Up_VK}
    VK Sign In


##todo C1514	Аккаунт: Регистрация нового пользователя через уже подключенный VK
#Test-1514-Log_in_VK
#    [Tags]                                         case_id_1514   fff
#    SeleniumLibrary.Create Webdriver                             Firefox
#    GoToStage
#    looch open application
#
##    WaitAndClick   ${Tapbar_profile}
##    WaitAndClick   ${Sing_Up}
##    WaitAndClick   ${Sing_Up_VK}
#    WaitAndClick   ${Tapbar_profile}
#    WaitAndClick  ${Log_In}
#    WaitAndClick  ${Log_In_VK}
#    VK Sign In
#    deleteaccount  Leo
#    ${XML}=  AppiumLibrary.Log Source
#
#
