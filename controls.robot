*** Settings ***
Library              AppiumLibrary
Resource             resource.robot
*** Variables ***
${Looch}    //XCUIElementTypeApplication[@name="Looch"]


${Tapbar}            ${Looch}/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeTabBar/XCUIElementTypeButton
${Tapbar_home}       ${Tapbar}[1]
${Tapbar_channels}   ${Tapbar}[2]
${Tapbar_games}      ${Tapbar}[3]
${Tapbar_search}     id=tаbbar[search]
${Tapbar_profile}    id=tаbbar[profile]

${main_carousel}     id=main_carousel
${bestgames}         id=main_bestgames_item[game:100001]
#games
${live}               id=Live
${Sing_Up_temp}        //XCUIElementTypeApplication[@name="Looch"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]  #todo
${Sing_Up}             id=btn_singup
${userMenu}            ${Looch}/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeOther #todo
${userMenu_Sing_Up}    ${userMenu}/XCUIElementTypeTable/XCUIElementTypeOther[1]/XCUIElementTypeButton[@name="Sign Up"] #todo
${userMenu_Log_in}     //XCUIElementTypeButton[@name="Log In"]  #todo
${userMenu_FeedBack}   ///XCUIElementTypeStaticText[@name="Feedback"]
${Settings}            ${userMenu}/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeStaticText[@name="Settings"]  #todo

${Sing_Up_Username}   ${Sing_Up_temp}/XCUIElementTypeOther[1]  #todo
${Sing_Up_Email}      ${Sing_Up_temp}/XCUIElementTypeOther[2]  #todo
${Sing_Up_Password}   ${Sing_Up_temp}/XCUIElementTypeOther[3]  #todo
${Sing_Up_VK}          id=btn_signup_social[vk]
${Sing_Up_FB}          id=btn_signup_social[fb]




#Channel
${Channel_login_to_chat}   id=Log In to chat…
${Channel_Video}           id=Video
${Channel_Menu_btn}                  xpath=//XCUIElementTypeApplication[@name="Looch"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeButton[1]
${Channel_Settings_btn}          xpath=//XCUIElementTypeApplication[@name="Looch"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[4]/XCUIElementTypeOther[2]/XCUIElementTypeButton[1]
${Channel_Userlist_btn}           xpath=//XCUIElementTypeApplication[@name="Looch"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[4]/XCUIElementTypeOther[2]/XCUIElementTypeButton[2]
${Channel_SaySomething_Field}    id=Say something
${Channel_Smile_btn}             xpath=//XCUIElementTypeApplication[@name="Looch"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeButton[2]
${Channel_Send_btn}              xpath=//XCUIElementTypeApplication[@name="Looch"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeButton[3]
${Channel_Chat_msg_cell}         xpath=//XCUIElementTypeApplication[@name="Looch"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeTable/XCUIElementTypeCell[1]
${Channel_UserMenu_Admin_btn}             xpath=//XCUIElementTypeApplication[@name="Looch"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[4]/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeButton[1]
${Channel_UserMenu_invisible_btn}         xpath=//XCUIElementTypeApplication[@name="Looch"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[4]/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeButton[2]
${Channel_UserMenu_ban_btn}               xpath=//XCUIElementTypeApplication[@name="Looch"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[4]/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeButton[3]
${Channel_UserMeny_Admin_icon}            xpath=//XCUIElementTypeApplication[@name="Looch"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeImage[2]

${web_Channel_Chat_Input}                 css=.e2e_input_chat
${web_Channel_Chat_Menu}                  css=.e2e_chat_menu_button
${web_Channel_Chat_Submit_Button}         css=.e2e_chat_submit_button


${Log_In}                  id=btn_singin
${Log_in_username}         id=text_login_username
${Log_in_Password}         id=text_login_password
${Log_in_ForgotPassword}   ${Log_In}/XCUIElementTypeButton[@name="Forgot password?"]
${Log_in_login_btn}        id=btn_login_login
${Log_in_ToS}              ${Log_In}/XCUIElementTypeStaticText[@name="By pressing Log In button you agree Looch's Terms of Service and Privacy Policy"][1]/XCUIElementTypeLink[@name="Terms of Service"]
${Log_in_Privacy}          ${Log_In}/XCUIElementTypeStaticText[@name="By pressing Log In button you agree Looch's Terms of Service and Privacy Policy"][1]/XCUIElementTypeLink[@name="Privacy Policy"]
${Log_In_VK}                  id=btn_login_social[vk]


${web_Login_Button}        css=[class*=UserMenu__loginButton]


${player}                     id=Video

${Search_recent_dota}         id=btn_recent_search[0:dota]
${Search_clear_resent}        id=btn_recent_clear
${Search_clear}               id=btn_search_clear
${Search_line}                id=text_search
${Search_games_seeall}        id=btn_games_seeall
${Search_channels_seeall}     id=btn_channels_seeall
${Search_streams_seeall}      id=btn_streams_seeall
${Search_videos_seeall}       id=btn_videos_seeall
${Search_anyGame}             btn_item[game
${Search_anyChannel}          btn_item[user
${Search_anyLive}             btn_item[channel
${Search_anyVideo}            btn_item[video
${Search_back}                id=btn_top[back]
${Search}                     name=Search





${Profile_Community}                     id=row_community



${Profile_Feedback}                      id=row_feedback
${Feedback_Submit}                       id=Submit

${Feedback_Subject_Select}               xpath=//XCUIElementTypeApplication[@name="Looch"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeTextField[3]
${Feedback_Feedback}                     xpath=//XCUIElementTypeButton[@name="Feedback"]
${Feedback_Text_Field}                   xpath=//XCUIElementTypeApplication[@name="Looch"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeTextView
${Feedback_Confirmation}                 id=OK

${Profile_UserChannel_btn}                 id=area_currentuser

${Profile_Settings}                        id=row_settings
${Profile_Settings_Language_English}       id=English
${Profile_Settings_Language_Russian}       id=Русский
${Profile_Settings_Logout}                 id=btn_logout
${Profile_Settings_Logout_dialog_Logout}   xpath=//XCUIElementTypeButton[@name="Log Out"]
${Profile_Settings_Logout_dialog_Cancel}   xpath=//XCUIElementTypeButton[@name="Cancel"]


${Settings_Language_Eng}                     id=Language
${Settings_Language_Rus}                     id=Язык интерфейса
${Settings_About_Looch_btn}                  id=About Looch
${Settings_About_Looch_page}                 id=Follow us:
${Settings_ToS_btn}                          id=Terms of Service
${Settings_PP_btn}                           id=Privacy Policy
${Settings_About_Looch_ToS_page}             id=BrowserWindow
${Settings_About_Looch_PP_page}              id=BrowserWindow



${Profile_Account_Settings}                    id=row_account-settings
${Profile_Account_Settings_Username}           id=//XCUIElementTypeApplication[@name="Looch"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]
${Profile_Account_Settings_Mail}               id=//XCUIElementTypeApplication[@name="Looch"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[4]/
${Profile_Account_Settings_Save}               xpath=//XCUIElementTypeButton[@name="Save"]
${Profile_Account_Settings_FB_btn}             xpath=//XCUIElementTypeButton[@name="Connect"][1]
${Profile_Account_Settings_VK_btn}             xpath=(//XCUIElementTypeButton[@name="Connect"])[2]
${Profile_Account_Settings_ChangePicture}      id=Change Picture
${Profile_Account_ResetPassword}               id=Change Picture


#Facebook
${Facebook_mail_field}                           xpath=//XCUIElementTypeOther[@name="main"]/XCUIElementTypeTextField
${Facebook_password_field}                       xpath=//XCUIElementTypeOther[@name="main"]/XCUIElementTypeSecureTextField
${Facebook_Login_btn}                            id=Log In
${Facebook_continue_as}                          xpath=//XCUIElementTypeOther[@name="main"]/XCUIElementTypeOther[5]
${Facebook_Connected_btn}                        id=Connected
${Facebook_Unlink}                               id = Unlink
${Facebook_Error}                                xpath=//XCUIElementTypeAlert[@name="Error"]
${Facebook_OK}                                   id=OK


#VK
${VK_mail_field}                                xpath=//XCUIElementTypeOther[@name="Gain Access to VK"]/XCUIElementTypeOther[4]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeTextField
${VK_password_field}                            xpath=//XCUIElementTypeOther[@name="Gain Access to VK"]/XCUIElementTypeOther[5]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeSecureTextField
${VK_Login_btn}                                 id=Log in
${VK_Connected_btn}                              id=Connected
${VK_Error}                                      xpath=//XCUIElementTypeAlert[@name="Error"]
${VK_Unlink}                                     id = Unlink
${VK_OK}                                         id=OK
${UserVK}        uriluchkov@yandex.ru
${PasswordVK}    vk_l00ch


#Community
${Followers}    id=Followers
${Following}    id=Following


#WEB
${Chat_Menu}             css=.e2e_chat_menu_button
${facebook_mail}             tom_wfnwqdd_staim@tfbnw.net
${facebook_password}     fb_l00ch
${user_name}             tom_wfnwqdd_staim
${Login_Button}          css=[class*=UserMenu__loginButton]
