*** Settings ***
Library              AppiumLibrary
Library              XML
Library              Collections
Library              ListParser
Library              DateTime
Library              SeleniumLibrary
Resource             resource.robot
Resource             controls.robot
Suite Setup
Test Setup
Test Teardown
Variables            ListParser.py
*** Variables ***
${REMOTE_URL}     http://0.0.0.0:4723/wd/hub
${platformName}  iOS
${platformVersion}  11.0
${xcodeSigningId}  iPhone Developer
${bundleId}    com.vispamedia.turbo.stage
${sendKeyStrategy}  setValue
${usePrebuiltWDA}  true
${wdaLocalPort}  8100
${xcodeOrgId}  VISPA MEDIA, OOO
${deviceName}  Iphone
${udid_iPhone7}  8bab36b32387df630d7a41d3b1309b5667539b12
${udid_iPhone7Plus}  87b25f63c04541ee074384880116eceac4f03d9f
${udid_iPhone6Plus}  870965dd92a1c41e7303005b5be83eb2979d095e

*** Keywords ***
open application and Registratiom
    Create Webdriver           Chrome
    ${User1_Follows_User3}=                         Browser Registration
    ${User1_Follows_User2}=                         Browser Registration
    looch open application
    Login          ${username1}
    GotoSearch
    input text       ${search_line}   dot
    sleep  100




FollowFromSearch
*** Test Cases ***
#todo С1322	Аккаунт: Сообщество: Подписки: Выключить нотификации

Test-1322-CommunityNotificationTurnOff
   [Tags]        case_id_1322
   GotoCommunity
   #todo Notification turn off


#todo С1323	Аккаунт: Сообщество: Подписки: Включить нотификации
Test-1323-CommunityNotificationTurnOn
   [Tags]        case_id_1323
   GotoCommunity
   #todo Notification turn on


#todo С1324	Аккаунт: Сообщество: Подписки: Отписка
Test-1324-CommunityUnFollowing
   [Tags]        case_id_1324
   GotoCommunity
   #todo Unfollow





#todo С1325	Аккаунт: Сообщество: Подписки: Переход на канал пользователя
Test-1325-CommunityFollowing
   [Tags]        case_id_1325
   GotoCommunity
   click element
   #todo Goto User Channel


#todo С1326	Аккаунт: Сообщество: Подписчики: Можно подписаться
Test-1325-CommunityFollowing
   [Tags]        case_id_1325
   GotoCommunity
   click element
   #todo Goto User Channel
#todo С1327	Аккаунт: Сообщество: Подписчики: Можно перейти на канал подписчика