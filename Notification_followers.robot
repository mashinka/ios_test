*** Settings ***
Library              AppiumLibrary
Library              XML
Library              Collections
Library              ListParser
Library              DateTime
Library              SeleniumLibrary
Resource             resource.robot
Resource             controls.robot
Suite Setup         open application and Registratiom
Suite Teardown      Close All Browsers


Variables            ListParser.py
*** Variables ***
${REMOTE_URL}     http://0.0.0.0:4723/wd/hub
${platformName}  iOS
${platformVersion}  11.0
${xcodeSigningId}  iPhone Developer
${bundleId}    com.vispamedia.turbo.stage
${sendKeyStrategy}  setValue
${usePrebuiltWDA}  true
${wdaLocalPort}  8100
${xcodeOrgId}  VISPA MEDIA, OOO
${deviceName}  Iphone
${udid_iPhone7}  8bab36b32387df630d7a41d3b1309b5667539b12
${udid_iPhone7Plus}  87b25f63c04541ee074384880116eceac4f03d9f
${udid_iPhone6Plus}  870965dd92a1c41e7303005b5be83eb2979d095e

*** Keywords ***
open application and Registratiom
    Create Webdriver           Chrome
    GoToStage
    ${UserB}=                         RegistrationBrowser
    LogoutWeb
    ${UserA}=                         RegistrationBrowser
    ChannelFollow     ${UserB.id}    ${UserA.token}
    looch open application
    Login          ${userB.name}
    sleep    1000


*** Test Cases ***

#todo С1326	Аккаунт: Сообщество: Подписчики: Можно подписаться
Test-1325-CommunityFollwers
   [Tags]        case_id_1325     lll
   GotoCommunity
   click element      ${Followers}


   #todo  Goto followers
   #Todo follow
   #Follow to followers


#todo С1327	Аккаунт: Сообщество: Подписчики: Можно перейти на канал подписчика
Test-1327-CommunityFollwers
   [Tags]        case_id_1325
   GotoCommunity
   #todo  Goto followers
   #todo Goto User Channel